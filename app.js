// Get Our Elements
const player = document.querySelector('.player');
const video = document.querySelector('.viewer');

const fullScr = document.querySelector('.full-scr');

const progress = document.querySelector('.progress');
const progressBar = document.querySelector('.progres-filled');

const mute = document.querySelector('.mute');
const toggle = document.querySelector('.toggle');

const skipBtn = player.querySelectorAll('[data-skip]');
const ranges = player.querySelectorAll('.player-slider')

// Build out Functions

function togglePlay() {
// const metod = video.paused ? 'play': 'pause';
// video[metod]();
    if(video.paused){
        video.play();
    } else{
        video.pause();
    }
}

function updateBtn() {
const icon = this.paused ? '►' : '❚ ❚';
toggle.textContent = icon;
}

function skip() {
    // console.log(this.dataset.skip);
    video.currentTime += parseFloat(this.dataset.skip);
}

function handleRangeUpdate() {
    video[this.name] = this.value;
    // console.log(this.name);
    // console.log(this.value);
}

function enableMute() { 
    if (video.muted){
        video.muted = false;
        mute.innerHTML = "Mute";
      } else{
        video.muted = true;
        mute.innerHTML = "Sound";
      }
} 

function handleProgress() {
    const percent = (video.currentTime / video.duration) * 100;
    progressBar.style.width = `${percent}%`;
}

function scrub(e) {
    const scrubTime = (e.offsetX / progress.offsetWidth) * video.duration;
    video.currentTime = scrubTime;
}

function fullScreen (){
    if (video.requestFullscreen) {
        video.requestFullscreen();
    } else {
        video.webkitRequestFullscreen();
    }
}

// Hook up the event listnners

video.addEventListener('click', togglePlay);
video.addEventListener('play', updateBtn);
video.addEventListener('pause', updateBtn);
video.addEventListener('timeupdate', handleProgress);

toggle.addEventListener('click', togglePlay);
skipBtn.forEach(button => button.addEventListener('click', skip));

ranges.forEach(range => range.addEventListener('change', handleRangeUpdate));
ranges.forEach(range => range.addEventListener('mousemove', handleRangeUpdate));

mute.addEventListener('click', enableMute);
progress.addEventListener('click', scrub);

fullScr.addEventListener('click', fullScreen);